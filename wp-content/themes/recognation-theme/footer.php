        <footer class = "recog-footer" id = "footer">
            <!-- top part above hr -->
            <div class = "container">

                <div class = "row">

                    <div class = "col">
                        <a href ="#home">
                            <img class = "logo-image" src = <?php echo get_theme_file_uri('css/images/White-Logo-Better.png') ?> alt = "Recognation Logo"> 
                        </a>
                    </div>
        
                    <div class = "col">
                        <!-- text transform all caps -->
                        <ul class = "text-upper k-font body-large-fs">
                            <li><a href = "#">Solutions</a></li>
                            <li><a href = "#">Why Recognation</a></li>
                            <li><a href = "#">Resources</a></li>
                            <li><a href = "#">Request A Demo</a></li>
                        </ul>
                    </div>
        
                    <div class = "col">
                        <ul class = "k-font body-large-fs">
                            <li><a href = "#">Legal</a></li>
                            <li><a href = "#">About</a></li>
                            <li><a href = "#">Careers</a></li>
                            <li><a href = "#">Contact Us</a></li>
                            <li><a href = "#">Support</a></li>
                        </ul>
                    </div>
        
                    <!-- is the button the link (if i wrap it in a tags) or is it the words -->
                    <div class = "col">
                        <ul>
                            <li class = "pls-fix-footer">
                                <!-- carrot! -->
                                <a target = "_blank" href = "https://trypap.com/">
                                    <button class = "footer-demo-btn button-font">Request A Demo
                                        <img src= <?php echo get_theme_file_uri('css/icons/material-icon/right_teal.png') ?> alt = "Right Chevron" class = "white-carrot-icon">
                                    </button>
                                </a>
                            </li>
                        </ul>

                        <ul>
                            <li class = "text-upper footer-span">Join Our Email List</li>
                            <li class = "footer-li">
                                <!-- border right to separate it from join -->
                                <span class = "footer-button-input">
                                    <input class = "footer-input" type = "text" placeholder= "Type email here">
                                    <!-- carrot and text transform -->
                                    <button class = "footer-join-btn button-font" onclick = "alert('On Wednesdays we wear pink.')">Join
                                        <img src= <?php echo get_theme_file_uri('css/icons/material-icon/right_teal.png') ?> alt = "Right Chevron" class = "white-carrot-icon">
                                    </button>
                                </span>   
                            </li>
                        </ul>
                    </div>
            
                </div>

            </div>
            
            <!-- bottom part below hr -->
            <div class = "container">

                <hr>

                <div class = "row">
                    <div class = "col">
                        <img src = <?php echo get_theme_file_uri('css/images/baudville_brands-(1).png') ?> alt = "Baudville Brands" class = "footer-logo-img">
                        <img src = <?php echo get_theme_file_uri('css/images/baudville-(1).png') ?> alt = "Baudville" class = "footer-logo-img">
                        <img src= <?php echo get_theme_file_uri('css/images/idville-(1).png') ?> alt = "Idville" class = "footer-logo-img">
                    </div>
                    <div class = "col-3">
                        <img src = <?php echo get_theme_file_uri('css/images/baudville_best_and_brightest.png') ?> alt = "Best and Greatest Companies" class = "footer-logo-img">
                        <img src = <?php echo get_theme_file_uri('css/images/Great-Place-to-Work-Logo-(1).png') ?> alt = "Best Place to Work" class = "footer-logo-img">
                    </div>
                </div>

                <p class = "footer-p">Powered by RecogNation, a Baudville company, &copy 2016 - 2021 RecogNation Inc, All rights reserved.</p>
                
            </div>
        
        </footer>
       <?php wp_footer(); ?>
       
    </body>
</html>