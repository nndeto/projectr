<!DOCTYPE html>
<!-- language_attributes indicates what language the file is to google -->
<html <?php language_attributes(); ?>>
    <head>

        <link rel="apple-touch-icon" sizes="180x180" href="wp-content/themes/recognation-theme/favicon_io/apple-touch-icon.png">
        <link rel="icon" type="image/png" sizes="32x32" href="wp-content/themes/recognation-theme/favicon_io/favicon-32x32.png">
        <link rel="icon" type="image/png" sizes="16x16" href="wp-content/themes/recognation-theme/favicon_io/favicon-16x16.png">
        <link rel="manifest" href="wp-content/themes/recognation-theme//favicon_io/site.webmanifest">
        <!-- <title>RecogNation</title> -->
        <!-- comment.. -->
        <!-- puts wordpress in charge of loading files and css -->
        <?php wp_head(); ?>
    </head>
    <body>  
       
        <header class = "container" id = "home">

            <nav class = "row navbar">

                <div class = "col">
                    <a href = "#footer">
                        <img class = "nav-logo col navbar-brand" src = <?php echo get_theme_file_uri('css/images/Rec-Logo-Teal.png') ?> alt = "Recognation Logo">
                    </a>
                </div>


                <ul class = "col nav-ul navbar">
                    <!-- text transform all caps -->
                    <!-- hover effect a -->
                    <li><a href ="https://amazondating.co/" target = "_blank" class = "nav-word">Solutions</a></li>
                    <li><a href ="http://endless.horse/"  target = "_blank" class = "nav-word">Why recognation</a></li>
                    <!-- carrot detail -->
                    <!-- will need background colors as well as gradient effect -->
                    <li class="dropdown">
                        <a class="nav-link dropdown-toggle nav-word" href="#" id="dropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Resources
                        <img src = <?php echo get_theme_file_uri('css/icons/material-icon/down_teal.png') ?> alt = "Drop Down Icon" class = "carrot-icon">
                        </a>
                        <!-- outline on hover -->
                        <!-- carrot detail here -->
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                        <a class="dropdown-item nav-word nav-dropdown-word" href="https://isitchristmas.com/" target = "_blank">Articles</a>
                        <a class="dropdown-item nav-word nav-dropdown-word" href="https://www.youtube.com/watch?v=dQw4w9WgXcQ/" target = "_blank">Downloads</a>
                        </div>
                    </li>
                        <div class = "nav-btn-wrapper">
                            <a href = "#"">
                                <button class = "button-font nav-btn" onclick = "alert('Get in loser, we\'re going shopping.')">Request a Demo
                                <img src= <?php echo get_theme_file_uri('css/icons/material-icon/right_teal.png')?> alt = "Right Chevron" class = "carrot-icon">
                                </button>
                            </a>
                        </div>
                    </li>
                </ul>

            </nav>

        </header>