/// GLOBAL VARIABLES ///

// var templateUrl = '<php? echo get_theme_file_uri(); ?>';

// Section Two Carousel
let autoRunInt = null;

// actual image that is supposed to change
let slideNumber = 0;

let caption = document.querySelector(".s-two-carousel-caption");

let carouselImg = document.querySelector(".s-two-carousel-img")
let imgSrcs = [
    'wp-content/themes/recognation-theme/css/expressions/first-day-envelope.png',
    'wp-content/themes/recognation-theme/css/expressions/party-confetti.png',
    'wp-content/themes/recognation-theme/css/expressions/hold-gift-box.png',
    'wp-content/themes/recognation-theme/css/expressions/high-five.png',
    'wp-content/themes/recognation-theme/css/expressions/toast-champagne-glass.png',
    'wp-content/themes/recognation-theme/css/expressions/performance-chart.png'
];

// caption array --> will be cycled through as carousel plays
let captions = [
    "First Days", 
    "Life Celebrations", 
    "Anniversaries", 
    "Everyday High-Fives", 
    "Retirement", 
    "Award-Winning Performance"
];

// indicator lights that will change color based on where interval is
let lightOne = document.getElementById("indicator-one");
let lightTwo = document.getElementById("indicator-two");
let lightThree = document.getElementById("indicator-three");
let lightFour = document.getElementById("indicator-four");
let lightFive = document.getElementById("indicator-five");
let lightSix = document.getElementById("indicator-six");

let indicatorArr = [
    lightOne,
    lightTwo,
    lightThree,
    lightFour,
    lightFive,
    lightSix
]

// control buttons
let leftPlay = document.getElementById("left-play-btn");
let pauseBtn = document.getElementById("pause-btn");
let rightPlay = document.getElementById("right-play-btn");


// Section Three Hover Aspect
let imageChangeDiv = document.querySelector(".s-three-change-img");
let hovZero = document.querySelector(".s-three-hov-zero");
let hovOne = document.querySelector(".s-three-hov-one");
let hovTwo = document.querySelector(".s-three-hov-two");
let hovThree = document.querySelector(".s-three-hov-three");


/// FUNCTIONS ///

// autopilot function for carousel --> an attempt was made  (╥_╥)
// function autoRun() {
//     let count = 0
//     while (count < 5) {
//         autoRunInt = setInterval(function() {
//             displayImage(count);
//             console.log(count)
//         }, 3000)
//     }
//     count++
//     console.log("Interval ended")
//     // setInterval()
// }

// will change the indicator lights
function turnOnLight(number) {
    for (let i = 0; i < indicatorArr.length; i++) {
        if (indicatorArr[i] != indicatorArr[number]) {
            indicatorArr[i].style.backgroundColor = "#FFFFFF40"
            continue
        } else {
            indicatorArr[number].style.backgroundColor = "#FFF";
        }
    }
}

// will change out the display and caption
function displayImage(number) {
    // console.log(number)
    turnOnLight(slideNumber);
    caption.textContent = captions[number];
    carouselImg.src = imgSrcs[number];
    return
}

// triggered by click and calls necessary sub functions
function leftClick(e) {
    // console.log(e)
    if (e.target.alt === "Left Play Button") {
        // checks and stops show if number exceeds array options
        if (slideNumber <= 0) {
            return
        } else if (slideNumber > 0) {
            // changes the global variable that will go into displayImage
            slideNumber--
            displayImage(slideNumber);
        }
    }
}

function rightClick(e) {
    if (e.target.alt === "Right Play Button") {
        if (slideNumber >= 5) {
            return
        } else if (slideNumber < 5) {
            slideNumber++
            displayImage(slideNumber);
        }
    }
}



function imgChangeZero() {
    // console.log("you hit me");
    imageChangeDiv.style.backgroundImage = "url('wp-content/themes/recognation-theme/css/images/section-three-gif.gif')";
}

function imgChangeOne() {
    imageChangeDiv.style.backgroundImage = "url('wp-content/themes/recognation-theme/css/images/section-three-img.png')";
}

function imgChangeTwo() {
    imageChangeDiv.style.backgroundImage = "url('wp-content/themes/recognation-theme/css/images/section-three-two.jpeg')";
}

function imgChangeThree() {
    imageChangeDiv.style.backgroundImage = "url('wp-content/themes/recognation-theme/css/images/section-three-second-gif.gif')";
}

function resetImg() {
    imageChangeDiv.style.backgroundImage = "url('wp-content/themes/recognation-theme/css/images/section-three-gif.gif')";
}




/// EVENT LISTENERS ///
leftPlay.addEventListener("click", leftClick);
rightPlay.addEventListener("click", rightClick);

hovZero.addEventListener("mouseenter", imgChangeZero);
hovOne.addEventListener("mouseenter", imgChangeOne);
hovTwo.addEventListener("mouseenter", imgChangeTwo);
hovThree.addEventListener("mouseenter", imgChangeThree);
hovOne.addEventListener("mouseout", resetImg);
hovTwo.addEventListener("mouseout", resetImg);
hovThree.addEventListener("mouseout", resetImg);

// window.addEventListener("load", autoRun);





// jquery Playaround
$(document).ready(function() {
    02
        $(window).scroll( function(){
    03
            $('.fade').each( function(i){
    04
                 
    05
                var bottom_of_element = $(this).offset().top + $(this).outerHeight();
    06
                var bottom_of_window = $(window).scrollTop() + $(window).height();
    07
                 
    08
                if( bottom_of_window > bottom_of_element ){
    09
                    $(this).animate({'opacity':'1'},1000);
    10
                }
    11
                 
    12
            });
    13
        });
    14
    });
    