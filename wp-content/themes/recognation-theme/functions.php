<!-- yayyyy we get to set up our css loading and funcitonality back here -->

<?php 
    
    // loading site files
    function site_files() {  
    
	    // wp_enqueue_script('jquery1', '//ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js', array(), '', true);
        // wp_enqueue_script('jquery2', '//code.jquery.com/jquery-3.2.1.slim.min.js', array(),'', true);

        // one jquery file to rule them all I guess. 
	    wp_enqueue_script('jquery1', 'https://code.jquery.com/jquery-3.1.0.js', array(), '', true);
        
        // my homegrown jscript
        wp_enqueue_script('main-js-file', get_theme_file_uri('js/app.js'), array('jquery'), '1.0', true); 
        
        // my fonts
        wp_enqueue_style('google_font1', '//fonts.googleapis.com/css?family=Titillium+Web:200,200italic,300,300italic,regular,italic,600,600italic,700,700italic,900)');
        wp_enqueue_style('google_font2', '//fonts.googleapis.com/css?family=Open+Sans:300,regular,500,600,700,800,300italic,italic,500italic,600italic,700italic,800italic)');

        // my bootstrap files 
        wp_enqueue_style('bootstrap_files', '//cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css' );
        wp_enqueue_script('bootstrap1',  '//cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js', array( 'jquery' ),'',true );
        wp_enqueue_script('bootstrap2',  '//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js', array( 'jquery' ),'',true );
        

        // my style sheet
        wp_enqueue_style('style_index', get_theme_file_uri('/css/style-index.css'));
    }


    add_action('wp_enqueue_scripts', 'site_files');
?>