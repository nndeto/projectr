<?php get_header(); ?>

    <!-- entire div will get background image -->
    <section class = "section-one" >
            <!-- block with words -->
            <div class = "container">

                    <div class = "section-one-border col-6">

                        <div class = "section-one-content-wrapper col-12">
                            <h1 class = "seventy-two-fs k-font">Maximize Every Moment<span class = "section-one-animate">.</span></h1>
                            <p class = "twenty-fs section-one-text">We help transform the way team members view and experience the workplace – 
                                recognizing every moment from the day they say “Hi” to the day they say “Bye”.
                            </p>
        
                            <div class = "row">
                                <div class = "col">
                                    <!-- carrot icon and outline effect -->
                                    <div class = "section-one-button-wrapper">
                                        <button class = "button-font section-one-btn" onclick= "alert('I\'m sorry that people are so jealous of me. But I can\'t help it that I\'m popular.')">Request A Demo
                                            <img src=  <?php echo get_theme_file_uri('css/icons/material-icon/right_teal.png') ?> alt = "Right Chevron" class = "carrot-icon">
                                        </button>
                                    </div>
                                </div>
        
                                <div class = "col">
                                    <div  class = "section-one-button-wrapper-one">
                                        <!-- will have download icon and download a guide  -->
                                        <a href="http://papertoilet.com/" target = "_blank">
                                            <button class = "button-font section-one-btn-one">Recognition Guide
                                                <img src= <?php echo get_theme_file_uri('css/icons/download.png') ?> class = "download-icon" alt = "Download icon">
                                            </button>
                                        </a>
                                    </div>
                                </div>
                            </div>

                        </div>
                        
                    </div>
            </div>

        </section>

        
        <!-- background color and mic icon-->
        <section class = "s-two-hidden-bg">

            <div class = "s-two">

                <img src = <?php echo get_theme_file_uri('css/expressions/mic-stand.png') ?> alt = "Mic Stand" class = "s-two-expression fade">
         
                <div class = "s-two-content-wrapper container">
                    <div class = "row">
                        <h2 class = "fifty-six-fs s-two-pad">It's time for a recognition revolution.</h2>
                            <p class = "general-font twenty-fs s-two-pad s-two-text">We believe the work experience needs a transformation. 
                                One where gratitude and appreciation are abundant, connection between 
                                people at all levels of the organization is strengthened, purpose and values are clear,
                                and inspiration and passion are on display. 
                            </p>
                    </div>
    
                 <!-- two divs so that I can split the carousel-->
                    <div class = "row s-two-subcontent">
    
                        <div class = "col">
                            <h4 class = "thirty-two-fs k-font s-two-pad s-two-h4">Even the small moments count.</h4>
                            <p class = "general-font s-two-pad s-two-text">We provide the platform, proactive nudges, real-time reporting, and the right mix of 
                                tangible products to help you make sure no moment goes unrecognized.
                            </p>
                            
                            <!-- carot idcons and hover effects -->
                            <div class = "s-two-btn-wrap ">
                                <div class = "s-two-btn-wrapper">
                                    <a href = "http://cat-bounce.com/" target = "_blank">
                                        <button class = "button-font s-two-btn ">Explore our solutions
                                            <img src= <?php echo get_theme_file_uri('css/icons/material-icon/right_teal.png') ?> alt = "Right Chevron" class = "carrot-icon">
                                        </button>
                                    </a>
                                </div>
    
                                <div>
                                    <a href = "http://www.staggeringbeauty.com/" target = "_blank">
                                        <button class = "button-font s-two-btn-one">Tell me more
                                            <img src= <?php echo get_theme_file_uri('css/icons/material-icon/right_teal.png') ?> alt = "Right Chevron" class = "white-carrot-icon">
                                        </button>
                                    </a>
                                </div>
                            </div>
    
    
                        </div>
    
                        <!-- Actual Carousel, rounded images, caption, indicator and arrows -->
                        <div class = "col-3 s-two-carousel-wrapper">

                            <div class = "s-two-carousel">
                                <img src = <?php echo get_theme_file_uri('css/expressions/first-day-envelope.png') ?> alt = "Hello" class = "s-two-carousel-img">
                                <p class = "s-two-carousel-caption">First Days</p>
                            </div>
    
                            <div class = "s-two-carousel-controls">
                               <span>
                                   <div class = "s-two-carousel-indicator" id = "indicator-one"></div>
                                   <div class = "s-two-carousel-indicator" id = "indicator-two"></div>
                                   <div class = "s-two-carousel-indicator" id = "indicator-three"></div>
                                   <div class = "s-two-carousel-indicator" id = "indicator-four"></div>
                                   <div class = "s-two-carousel-indicator" id = "indicator-five"></div>
                                   <div class = "s-two-carousel-indicator" id = "indicator-six"></div>
                               </span>

                               <br>
                               
                                <div>
                                    <button class = "s-two-carousel-btn" id = "left-play-btn">
                                        <img src= <?php echo get_theme_file_uri('css/icons/trythis-left.svg') ?> alt = "Left Play Button" width = "20" class = "s-two-filter-pic">
                                    </button>
                                    <button class = "s-two-carousel-btn" id = "pause-btn">
                                        <img src= <?php echo get_theme_file_uri('css/icons/trythis-pause.svg') ?>  alt = "Pause Button" width = "20" class = "s-two-filter-pic">
                                    </button>
                                    <button class = "s-two-carousel-btn" id = "right-play-btn">
                                        <img src= <?php echo get_theme_file_uri('css/icons/trythis-right.svg') ?>  alt = "Right Play Button" width = "20" class = "s-two-filter-pic">
                                    </button>
                                </div>

                            </div>

                        </div>

                    </div>
                </div>

            </div>

        </section>
    


        <!-- lots of icons pay attention -->
        <section class = "s-three">

            <img src = <?php echo get_theme_file_uri('css/expressions/decoration_starburst.png') ?> alt = "Firework Expression" class = "s-three-expressions s-three-e-zero fade">
            <img src = <?php echo get_theme_file_uri('css/expressions/gears.png') ?> alt = "Gears Expression" class = "s-three-expressions s-three-e-one fade">
            <img src = <?php echo get_theme_file_uri('css/expressions/team-yay-pennants.png') ?> alt = "Team Pennant Expression" class = "s-three-expressions s-three-e-two fade">

            <div class = "container">

                <div class = "row s-three-top-content">
                    <div>
                        <h2 class = "k-font fifty-six-fs">The RecogNation process.</h2>
                        <h6 class = "general-font twenty-fs s-three-p">We know that single-solution, one-size-fits-all programs just don’t work. 
                            That’s why we’ve designed an all-in-one, comprehensive approach that goes way 
                            beyond the one-track function of other recognition platforms and programs. 
                        </h6>
                    </div>
                </div>

                <div class = "row">
                    <!-- image and clicker changer on right side -->
                    <div class = "col">
                        <div class = "s-three-change-img"> </div>
                        <div class = "s-three-btn-wrap col-10">
                            <div class = "s-three-btn-wrapper">
                                <a href = "https://corgiorgy.com/" target = "_blank">
                                    <button class= "button-font s-three-btn">See How We Do It
                                        <img src= <?php echo get_theme_file_uri('css/icons/material-icon/right_teal.png') ?> alt = "Right Chevron" class = "carrot-icon">
                                    </button>
                                </a>
                            </div>
                        </div>

                    </div>
    
    
                    <!-- right side clicker change menu extend to the left when clicked(maybe be fancy and do a hover) -->
                    <!-- Not even gonna lie, idk how this works, I was just messing around trying to make it work and i hacked this together ¯\_(ツ)_/¯ -->
                    <div class = "col container-fluid">
                        <!-- check if bullets can be replaced with pics -->
                        <!-- you can indeed, check reference doc in google -->
                        <ul>
                            <div class = "s-three-options">
                                <li class = "s-three-li s-three-hov-zero caption-subtitle-fs"><div><img class = "s-three-li-img-zero" src= <?php echo get_theme_file_uri('css/RecogNation-Brand-System-(3)/brand-icon/icon_wellness.png') ?> alt = "Wellness"></div>Create authentic, meaningful, <br> and engaging moments of <br> expressing gratitude.</li>
                            </div>
                            <br>
                            <br>
                            <br>
                            <br>
                            <br>
                            <div class = "s-three-options">
                                <li class = "s-three-li s-three-hov-one caption-subtitle-fs"><div><img class = "s-three-li-img-one" src= <?php echo get_theme_file_uri('css/RecogNation-Brand-System-(3)/brand-icon/icon_employee.png') ?> alt = "Employees"></div>Empower peers to encourage <br> and uplift each other.</li>
                            </div>
                            <br>
                            <br>
                            <br>
                            <br>
                            <br>
                            <div class = "s-three-options">
                                <li class = "s-three-li s-three-hov-two caption-subtitle-fs" id = "s-three-mess-zero"><div><img class = "s-three-li-img-two" src= <?php echo get_theme_file_uri('css/RecogNation-Brand-System-(3)/brand-icon/icon_nominations.png') ?> alt = "Nominations"></div>Reinforce the values that set <br> your culture apart from others.</li>
                            </div>
                            <br>
                            <br>
                            <br>
                            <br>
                            <br>
                            <div class = "s-three-options">
                                <li class = "s-three-li s-three-hov-three caption-subtitle-fs" id = "s-three-mess-one"><div><img class = "s-three-li-img-three" src= <?php echo get_theme_file_uri('css/RecogNation-Brand-System-(3)/brand-icon/icon_bottom-line.png') ?> alt = "Thumbs UP"></div>Foster connections in all areas <br> of your organization.</li>     
                            </div>
                        </ul>
       
                    </div>
                    
                </div>
                
            </div>

        </section>

    
        <section class = "section-four">

            <img src = <?php echo get_theme_file_uri('css/expressions/lightbulb.png') ?> alt = "Lightbulb" class = "s-four-expression fade">
            <img src= <?php echo get_theme_file_uri('css/expressions/team-megaphone.png') ?> alt = "Team Megaphone" class = "s-four-expression-one fade">


            <div class = "container">
                <div class = "row">
                    <h2 class = "fifty-six-fs k-font section-four-title">Engagement speaks for itself.</h2>
                    <h6 class="general-font twenty-fs section-four-subwords">Companies that improved their workplace culture and employee engagement using an effective recognition program have experienced:</h6>
                </div>
                
                <div>
                    <ul class = "section-four-ul row">
                        <div class = "col-4">
                            <li class = "section-four-number">17%</li>  
                            <li class = "section-four-li">incresed productivity*</li>
                        </div>
    
                        <div class = "col-4">
                            <li class = "section-four-number">24%</li>  
                            <li class = "section-four-li">incresed retention*</li>
                        </div>

                        <div class = "col-4">
                            <li class = "section-four-number">21%</li>  
                            <li class = "section-four-li">incresed profitability*</li>
                        </div>
                    </ul>
                </div>

                <!-- image cards with opacity stuff -->
                <div class = "row">

                    <div class="card col">
                        <img src = <?php echo get_theme_file_uri('css/images/stock-comp.jpg') ?> alt = "People working at computer"  class = "card-img-top">
                        <div class = "card-body">
                            <h3 class = "card-title k-font twenty-four-fs">A Comprehensive Guide To Starting A Recognition Program</h3>
                            <p class = "card-text general-font">Download our recognition guide and discover the process that's transforming 
                                company cultures all over the world.
                            </p>
                            <!-- download icon here -->
                            <div class = "s-four-button-wrapper">
                                <a href = "https://zoomquilt.org/" target = "_blank">
                                    <button class = "s-four-btn button-font">Download
                                        <img src= <?php echo get_theme_file_uri('css/icons/download.png') ?> class = "download-icon" alt = "Download icon">
                                    </button>
                                </a>
                            </div>
                        </div>
                    </div>
            
                    <div class="card col">
                        <img src = <?php echo get_theme_file_uri('css/images/stock-comp-two.jpg') ?> alt = "People Working at Computer" class="card-img-top" >
                        <div class = "card-body"> 
                            <h3 class = "card-title k-font twenty-four-fs">Take Your Service Awards To The Next Level</h3>
                            <p class = "card-text general-font">Download our white paper and explore all the ways you can elevate the service
                                awards in your company.
                            </p>
                           <!-- download icon here -->
                           <div class = "s-four-button-wrapper">
                               <a href = "http://make-everything-ok.com/" target = "_blank">
                                   <button class = "s-four-btn button-font">Download
                                        <img src= <?php echo get_theme_file_uri('css/icons/download.png') ?> class = "download-icon" alt = "Download icon">
                                   </button>
                               </a>
                           </div>
                        </div>
                    </div>

                </div>

                <div class = "general-font section-four-p">*Based on the Gallup State of the American Workplace Report, 2017</div>
                
            </div>

        </section>

        
        <section class = "section-five">

            <div class = "container">
                <div class = "row">
                    <h2 class = "k-font fifty-six-fs section-five-title">Our RecogNation community.</h2>
                    <p class = "general-font body-standard-fs section-five-words">No matter how large or small, we’re here for our awesome clients – 
                        transforming company cultures, providing constant support, and 
                        earning our community’s trust each and every day.</p>
                </div>

                <div class = "row-6">
                    <img src= <?php echo get_theme_file_uri('css/brand-logos/Arbys_White-01.png') ?> alt = "Arby's Logo" class = "section-five-img">
                    <img src= <?php echo get_theme_file_uri('css/brand-logos/SpectrumHealth_White-01.png') ?> alt = "Spectrum Logo" class = "section-five-img">
                    <img src = <?php echo get_theme_file_uri('css/brand-logos/NeilsenIQ_White-01.png') ?> alt = "Nielsen's Logo" class = "section-five-img">
                    <img src = <?php echo get_theme_file_uri('css/brand-logos/Carters_White-01.png') ?> alt = "Carter's Logo" class = "section-five-img">
                    <img src = <?php echo get_theme_file_uri('css/brand-logos/BobsDiscountFurniture_White-01.png') ?> alt = "Bob's Discount Furniture" class = "section-five-img">
                </div>

                <div class = "row-6">
                    <img src = <?php echo get_theme_file_uri('css/brand-logos/CrayolaOval_White-01.png') ?> alt = "Crayola Logo" class = "section-five-img">
                    <img src = <?php echo get_theme_file_uri('css/brand-logos/DHL_White-01.png') ?> alt = "DHL Logo" class = "section-five-img">
                    <img src = <?php echo get_theme_file_uri('css/brand-logos/KForce_White-01.png') ?> alt = "K-Force Logo" class = "section-five-img">
                    <img src = <?php echo get_theme_file_uri('css/brand-logos/IndependentBank_White-01.png') ?> alt = "Be Independent Logo" class = "section-five-img">
                    <img src = <?php echo get_theme_file_uri('css/brand-logos/GunLakeCasino_White-01-1.png') ?> alt = "Gun Lake Logo" class = "section-five-img"> 
                </div>
            </div>

        </section>

    
        <!-- Two Parts, two parts -->
        <section class = "section-six container">

            <img src = <?php echo get_theme_file_uri('css/expressions/trophy_number1.png') ?> class = "s-six-img fade" >
            <img src = <?php echo get_theme_file_uri('css/expressions/phone-messages.png') ?> class = "s-six-img-one fade" >

            <div class = "row">
                <h2 class = "k-font fifty-six-fs">Join the RecogNation movement.</h2>
                <p class = "general-font twenty-fs">Become a part of the recognition revolution that is elevating company cultures 
                    and employee experiences.
                </p>
            </div>
            
            <div>

                <ul class = "row s-six-ul s-six-ul-pad">
                    <div class = "col">
                        <li class = "col s-six-li k">500,000</li>
                        <li class = "s-six-text">active users</li>
                    </div>
                    <div class = "col">
                        <li class = "col s-six-li">100+</li>  
                        <li class = "s-six-text">countries</li>
                    </div>
                    <div class = "col">
                        <li class = "col s-six-li">90+</li>  
                        <li class = "s-six-text">languages</li>
                    </div>
                </ul>

                <ul class = "row s-six-ul">     
                    <div class = "col">
                        <li class = "col s-six-li">98.5%</li>  
                        <li class = "s-six-text">customer satisfaction</li>
                    </div>
                    <div class = "col">
                        <li class = "col s-six-li">99.94%</li>  
                        <li class = "s-six-text">system uptime</li>
                    </div>
                    <div class = "col">
                        <li class = "col s-six-li">100,000+</li>  
                        <li class = "s-six-text">rewards delivered annually</li>
                    </div>
                </ul>

            </div>

        </section>
    
        <!-- longest homepage ever, but you're so close! -->
    
        <!-- try photoshop to get ol' dude on this without having to play with split divs -->
        <section class = "section-seven">

            <img src = <?php echo get_theme_file_uri('css/expressions/cloud.png') ?> alt = "Handdrawn Cloud" class = "cloud-image fade" >
            <img src = <?php echo get_theme_file_uri('css/expressions/cloud_2.png') ?> alt = "Handdrawn Cloud" class = "cloud-image-two fade" >

            <div class = "container seven-div">

                <div class = "row">
                    <div class = "col s-seven-content">
                        <h4 class = "k-font thirty-two-fs">Upgrade your company culture with the help of our integrated technology and
                             comprehensive solutions.
                        </h4>

                        <br>

                         <!--carrot icon on thissss one and outline effect  -->
                        <div class = "s-seven-button-wrapper">
                            <button class = "button-font s-seven-btn" onclick = "alert('Made out with a hot dog? Oh my God that was one time!')">See how we do it
                                <img src= <?php echo get_theme_file_uri('css/icons/material-icon/right_teal.png') ?> alt = "Right Chevron" class = "carrot-icon">
                            </button>
                        </div>
                    </div>
                    
                    <div class = "col">
                        <img src =  <?php echo get_theme_file_uri('css/images/remove-bg-s7.png') ?> alt = "Man with Ipad" class = "s-seven-img align-baseline"> 
                    </div>
                </div>

            </div>

        </section>

<?php get_footer(); ?>